select
    zipcode,
    agency_responsible,
    status_notes,
    this.requested_datetiem as request_at,
    that.requested_datetime as next_request_at
from
    phl311 this
    left asof join phl311 that
        on (this.zipcode = that.zipcode
            and this.agency_responsible = that.agency_responsible
            and this.status_notes = that.status_notes
            and this.requested_datetime <= that.requested_datetime)
where zipcode = 19147
