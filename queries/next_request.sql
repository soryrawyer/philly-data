select
    zipcode,
    agency_responsible,
    status_notes,
    requested_datetime,
    lead(requested_datetime, 1)
        over (partition by
            zipcode,
            agency_responsible,
            status_notes
            order by requested_datetime asc)
from
    phl311
where zipcode = 19147
