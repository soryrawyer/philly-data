-- get the time to completion and the time past expected completion
-- the timestamp subtraction returns an interval data type
select
    zipcode,
    agency_responsible,
    status_notes,
    closed_datetime - requested_datetime as open_duration,
    expected_datetime - closed_datetime as past_due_duration
from
    phl311
