-- select open queries per day
with dated_requests as (
    -- truncate open and closed datetimes to just dates for a cleaner interval
    select
        zipcode,
        agency_responsible,
        status_notes,
        expected_datetime,
        date(requested_datetime) as request_date,
        date(closed_datetime) as closed_date
    from
        phl311
)

select
    zipcode,
    agency_responsible,
    status_notes,
    generate_series(
        request_date, closed_date, interval '1' day
    )::date as request_open_date
from
    dated_requests
